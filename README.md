# Product Manager

## A project to manage any kind of products

## Current progress

### DONE

-   Scripts for managing and dockerizing the PostreSQL database
-   ORM initalialization
-   Saving and securing user data
-   Authorization and authentication (PassportJS + JWT access)
-   Add product management logic
-   JWT access tokens -> JWT refresh tokens

### TODO

-   Make DB use NGINX to achieve HA
-   Make server use NGINX to achieve HA

### OPTIONAL

-   Make a simple UI with ReactJS
-   Add user permission restrictions

## Technologies and frameworks used

### Authentication and Authorization

-   PassportJS
-   JWT access tokens

### Database

-   PostreSQL + Docker

### Testing

-   Jest e2e test + Pactum
-   Postman (will provide endpoint tests when project is finished)
