import { Test } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as pactum from 'pactum';
import { PrismaService } from '../src/prisma/prisma.service';
import { AuthDto } from 'src/auth/dto';
import { EditUserDto } from 'src/user/dto';
import { CreateProductDto, EditProductDto } from 'src/product/dto';

describe('App e2e', () => {
    let app: INestApplication;
    let prisma: PrismaService;

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleRef.createNestApplication();
        app.useGlobalPipes(
            new ValidationPipe({
                whitelist: true,
            }),
        );

        await app.init();
        await app.listen(3333);

        prisma = app.get(PrismaService);
        await prisma.cleanDb();

        pactum.request.setBaseUrl('http://localhost:3333');
    });

    afterAll(() => {
        app.close();
    });

    describe('Auth', () => {
        const dto: AuthDto = {
            email: 'test@gmail.com',
            password: '123',
        };
        describe('Signup', () => {
            it('should signup', () => {
                return pactum
                    .spec()
                    .post('/auth/signup')
                    .withBody(dto)
                    .expectStatus(201);
            });

            it('should throw email already in use', () => {
                return pactum
                    .spec()
                    .post('/auth/signup')
                    .withBody(dto)
                    .expectStatus(403);
            });

            it('should throw invalid email', () => {
                return pactum
                    .spec()
                    .post('/auth/signup')
                    .withBody({
                        email: 'test@gmail',
                        password: dto.password,
                    })
                    .expectStatus(400);
            });

            it('should throw invalid email', () => {
                return pactum
                    .spec()
                    .post('/auth/signup')
                    .withBody({
                        password: dto.password,
                    })
                    .expectStatus(400);
            });

            it('should throw invalid password', () => {
                return pactum
                    .spec()
                    .post('/auth/signup')
                    .withBody({
                        email: dto.email,
                        password: '',
                    })
                    .expectStatus(400);
            });

            it('should throw invalid password', () => {
                return pactum
                    .spec()
                    .post('/auth/signup')
                    .withBody({
                        email: dto.email,
                    })
                    .expectStatus(400);
            });
        });

        describe('Signin', () => {
            it('should signin', () => {
                return pactum
                    .spec()
                    .post('/auth/signin')
                    .withBody(dto)
                    .expectStatus(200)
                    .stores('bearerToken', 'access_token');
            });

            it('should throw invalid email', () => {
                return pactum
                    .spec()
                    .post('/auth/signin')
                    .withBody({
                        email: 'test@gmail',
                        password: dto.password,
                    })
                    .expectStatus(400);
            });

            it('should throw invalid email', () => {
                return pactum
                    .spec()
                    .post('/auth/signin')
                    .withBody({
                        password: dto.password,
                    })
                    .expectStatus(400);
            });

            it('should throw invalid password', () => {
                return pactum
                    .spec()
                    .post('/auth/signin')
                    .withBody({
                        email: dto.email,
                        password: '',
                    })
                    .expectStatus(400);
            });

            it('should throw invalid password', () => {
                return pactum
                    .spec()
                    .post('/auth/signin')
                    .withBody({
                        email: dto.email,
                    })
                    .expectStatus(400);
            });
        });
    });

    describe('User', () => {
        describe('Get me', () => {
            it('should get user', () => {
                return pactum
                    .spec()
                    .get('/users/me')
                    .withHeaders({
                        Authorization: 'Bearer $S{bearerToken}',
                    })
                    .expectStatus(200);
            });
        });
        describe('Edit user', () => {
            const dto: EditUserDto = {
                firstName: 'Test123',
                email: 'test2@gmail.com',
            };
            it('should edit user', () => {
                return pactum
                    .spec()
                    .patch('/users')
                    .withHeaders({
                        Authorization: 'Bearer $S{bearerToken}',
                    })
                    .withBody(dto)
                    .expectStatus(200)
                    .expectBodyContains(dto.firstName)
                    .expectBodyContains(dto.email);
            });
        });
    });

    describe('Products', () => {
        const dto: CreateProductDto = {
            name: 'TestProduct1',
            description: 'Best product on the market',
        };

        const dto2: CreateProductDto = {
            name: 'TestProduct2',
        };

        describe('Get empty products by user', () => {
            it('should get empty products', () => {
                return pactum
                    .spec()
                    .get('/products')
                    .withHeaders({
                        Authorization: 'Bearer $S{bearerToken}',
                    })
                    .expectStatus(200)
                    .expectBody([]);
            });
        });

        describe('Create product', () => {
            it('should create a product', () => {
                return pactum
                    .spec()
                    .post('/products')
                    .withHeaders({
                        Authorization: 'Bearer $S{bearerToken}',
                    })
                    .withBody(dto)
                    .expectStatus(201)
                    .expectBodyContains(dto.name)
                    .expectBodyContains(dto.description)
                    .stores('productId', 'id');
            });

            it('should create a product without a description', () => {
                return pactum
                    .spec()
                    .post('/products')
                    .withHeaders({
                        Authorization: 'Bearer $S{bearerToken}',
                    })
                    .withBody(dto2)
                    .expectStatus(201)
                    .expectBodyContains(dto2.name);
            });
        });

        describe('Get products by user', () => {
            it('should get product', () => {
                return pactum
                    .spec()
                    .get('/products')
                    .withHeaders({
                        Authorization: 'Bearer $S{bearerToken}',
                    })
                    .expectStatus(200)
                    .expectJsonLength(2);
            });
        });

        describe('Get product by id', () => {
            it('should get product by id', () => {
                return pactum
                    .spec()
                    .get('/products/{id}')
                    .withPathParams('id', '$S{productId}')
                    .withHeaders({
                        Authorization: 'Bearer $S{bearerToken}',
                    })
                    .expectStatus(200)
                    .expectBodyContains('$S{productId}');
            });
        });

        describe('Edit product by id', () => {
            const editDto: EditProductDto = {
                description: 'Test123',
            };
            it('should edit product by id', () => {
                return pactum
                    .spec()
                    .patch('/products/{id}')
                    .withPathParams('id', '$S{productId}')
                    .withHeaders({
                        Authorization: 'Bearer $S{bearerToken}',
                    })
                    .withBody(editDto)
                    .expectStatus(200)
                    .expectBodyContains(editDto.description);
            });
        });

        describe('Delete product by id', () => {
            it('should delete product by id', () => {
                return pactum
                    .spec()
                    .delete('/products/{id}')
                    .withPathParams('id', '$S{productId}')
                    .withHeaders({
                        Authorization: 'Bearer $S{bearerToken}',
                    })
                    .expectStatus(200);
            });

            it('should get 1 product', () => {
                return pactum
                    .spec()
                    .get('/products')
                    .withHeaders({
                        Authorization: 'Bearer $S{bearerToken}',
                    })
                    .expectStatus(200)
                    .expectJsonLength(1)
                    .expectBodyContains(dto2.name)
                    .inspect();
            });
        });
    });
});
