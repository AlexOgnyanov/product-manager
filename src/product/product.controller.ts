import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Patch,
    Post,
    UseGuards,
} from '@nestjs/common';
import { AtGuard } from '../auth/guard';
import { ProductService } from './product.service';
import { GetUser } from '../auth/decorator';
import { EditProductDto, CreateProductDto } from './dto';

@UseGuards(AtGuard)
@Controller('products')
export class ProductController {
    constructor(private productService: ProductService) {}

    @Post()
    createProduct(
        @GetUser('id') userId: number,
        @Body() dto: CreateProductDto,
    ) {
        return this.productService.createProduct(userId, dto);
    }

    @Get()
    getProducts(@GetUser('id') userId: number) {
        return this.productService.getProducts(userId);
    }

    @Get(':id')
    getProductById(
        @GetUser('id') userId: number,
        @Param('id', ParseIntPipe) productId: number,
    ) {
        return this.productService.getProductById(userId, productId);
    }

    @Patch(':id')
    editProductById(
        @GetUser('id') userId: number,
        @Param('id', ParseIntPipe) productId: number,
        @Body() dto: EditProductDto,
    ) {
        return this.productService.editProductById(userId, productId, dto);
    }

    @Delete(':id')
    deleteProductById(
        @GetUser('id') userId: number,
        @Param('id', ParseIntPipe) productId: number,
    ) {
        return this.productService.deleteProductById(userId, productId);
    }
}
