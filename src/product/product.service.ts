import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { EditProductDto, CreateProductDto } from './dto';

@Injectable()
export class ProductService {
    constructor(private prisma: PrismaService) {}

    async createProduct(userId: number, dto: CreateProductDto) {
        const bookmark = await this.prisma.product.create({
            data: {
                userId,
                ...dto,
            },
        });

        return bookmark;
    }

    getProducts(userId: number) {
        return this.prisma.product.findMany({
            where: {
                userId,
            },
        });
    }

    getProductById(userId: number, productId: number) {
        return this.prisma.product.findFirst({
            where: {
                id: productId,
                userId,
            },
        });
    }

    async editProductById(
        userId: number,
        productId: number,
        dto: EditProductDto,
    ) {
        const product = await this.prisma.product.findUnique({
            where: {
                id: productId,
            },
        });

        if (!product || product.userId !== userId) {
            throw new ForbiddenException('Access to resources denied');
        }

        return this.prisma.product.update({
            where: {
                id: productId,
            },
            data: {
                ...dto,
            },
        });
    }

    async deleteProductById(userId: number, productId: number) {
        const product = await this.prisma.product.findUnique({
            where: {
                id: productId,
            },
        });

        if (!product || product.userId !== userId) {
            throw new ForbiddenException('Access to resources denied');
        }

        await this.prisma.product.delete({
            where: {
                id: productId,
            },
        });

        return product;
    }
}
